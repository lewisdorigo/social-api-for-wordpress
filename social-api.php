<?php
/**
 * Plugin Name:       Social API
 * Plugin URI:        https://bitbucket.org/lewisdorigo/social-api-for-wordpress
 * Description:       Twitter authentication and feed plugin
 * Version:           2.0.0
 * Author:            Lewis Dorigo
 * Author URI:        https://dorigo.co/
 */

namespace Dorigo\SocialAPI;


if(is_dir(__DIR__.'/vendor')) {
    require_once __DIR__.'/vendor/autoload.php';
}

define('DRGO_SOCIAL_API_PLUGIN', __FILE__);
define('DRGO_SOCIAL_API_PLUGIN_DIR', __DIR__);

Settings::getInstance();