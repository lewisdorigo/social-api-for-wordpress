<div class="wrap">
    <h2><?= $settings::PAGE_TITLE; ?></h2>

    <form action="options.php" method="post">
        <?php settings_fields($settings::MENU_SLUG); ?>

        <?php foreach($settings::NETWORKS as $network => $options): ?>
            <h3><?= $options['label']; ?></h3>
            <?= wpautop($options['description']); ?>

            <table class="form-table">
                <tbody>
                <?php foreach($options['fields'] as $field => $label): ?>
                    <?php $key = $settings->getSettingKey($network, $field); ?>
                    <tr>
                        <th scope="row">
                            <label for="<?= $key; ?>"><?= $label; ?></label>
                        </th>

                        <td>
                            <input type="text" name="<?= $key; ?>" id="<?= $key; ?>" value="<?= $settings->getOption($network, $field); ?>" class="regular-text ltr">
                        </td>
                    </tr>
                <?php endforeach; ?>
                </tbody>
            </table>

            <hr>
        <?php endforeach; ?>

        <?php submit_button(); ?>
    </form>
</div>