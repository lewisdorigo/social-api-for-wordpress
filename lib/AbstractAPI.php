<?php namespace Dorigo\SocialAPI;

use Dorigo\Singleton\Singleton;
use Dorigo\SocialAPI\Settings;

abstract class AbstractAPI extends Singleton {

    protected $client;

    protected $cacheDirectory;
    protected $cacheExpiry = 3600;
    protected $cachePath;
    protected $type;

    protected $settings;

    protected function __construct($cacheExpiry = 3600, $type = null) {
        $this->cacheExpiry = $cacheExpiry;

        if($type === null) {
            trigger_error('Could not get the API type.');
            die();
        }

        $this->settings = Settings::getInstance();
        $this->type = strtolower($type);

        $this->setAPIKeys();

        if(!$this->checkAPIKeys()) {
            throw new \Exception('The API Keys were not found');
        }
        $this->setClient();
    }

    public function getType(){
        return $this->type;
    }

    public function getOption(string $field, $default = null) {
        return $this->settings->getOption($this->type, $field, $default);
    }

    abstract public function request(string $endpoint, array $options = []) : array;

    abstract protected function setClient() : void;

    abstract protected function setAPIKeys() : void;
    abstract protected function checkAPIKeys() : bool;

    protected function getClient() {
        return $this->client;
    }

    protected function cachedRequest(string $endpoint, array $options = []) : array {

        $key = 'social-request-'.md5($endpoint.serialize($options));
        $posts = get_transient($key);

        if(!$posts) {
            $posts = $this->request($endpoint, $options);
            set_transient($key, $posts, $this->cacheExpiry);
        }

        $posts = apply_filters("Dorigo/Social/Cache/".$this->type, $posts, get_class(), $endpoint, $options);
        $posts = apply_filters("Dorigo/Social/Cache", $posts, get_class(), $endpoint, $options);

        return $posts;
    }
}