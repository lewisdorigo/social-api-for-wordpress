<?php namespace Dorigo\SocialAPI;

use Dorigo\Singleton\Singleton;

class Settings extends Singleton {

    const PAGE_TITLE = 'Social API Settings';
    const MENU_TITLE = 'Social API';
    const MENU_SLUG  = 'drgo-social-api';

    const NETWORKS = [
        'twitter' => [
            'label' => 'Twitter',
            'description' => 'You can create an app, and get API information from <a href="https://dev.twitter.com/" target="_blank">dev.twitter.com</a>.',
            'fields' => [
                'api_key' => 'API Key',
                'secret_key' => 'API Secret',
            ]
        ],
    ];

    const SETTING_PREFIX = 'drgo-social';

    protected function __construct() {

        add_action('admin_menu', [$this, 'addMenuItem']);
        add_action('admin_init', [$this, 'registerSettings']);

    }

    public function addMenuItem() {

        add_options_page(self::PAGE_TITLE, self::MENU_TITLE, 'manage_options', self::MENU_SLUG ,[$this, 'outputSettings']);

    }


    public function registerSettings() {

        foreach(self::NETWORKS as $network => $settings) {

            foreach($settings['fields'] as $field => $label) {

                register_setting(self::MENU_SLUG, $this->getSettingKey($network, $field));

            }

        }
    }

    public function getSettingKey(string $network, string $field) {

        return "{self::SETTING_PREFIX}-{$network}-{$field}";

    }

    public function getOption(string $network, string $field, $default = null) {

        $key = $this->getSettingKey($network, $field);
        return get_option($key) ?: $default;

    }

    public function outputSettings() {

        $settings = $this;
        require_once DRGO_SOCIAL_API_PLUGIN_DIR.'/templates/settings.php';

    }

}
