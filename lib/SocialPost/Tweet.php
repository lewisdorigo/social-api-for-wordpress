<?php namespace Dorigo\SocialAPI\SocialPost;

class Tweet {
    protected $id;
    protected $tweet;
    protected $retweet;
    protected $quote;
    protected $user;
    protected $media = [];
    protected $time;


    public function __construct($tweet) {

        $this->parseTweet($tweet);

    }

    public function parseTweet($tweet) : void {

        $this->id = $tweet->id;
        $this->media = [];
        $this->time = new \DateTime($tweet->created_at);

        $replacements = [];

        foreach($tweet->entities as $type => $entities) {

            foreach($entities as $entity) {
                $temp = [
                    'start' => (int) $entity->indices[0],
                    'end' => (int) $entity->indices[1],
                    'replacement' => ''
                ];

                switch($type) {
                    case 'media':
                    case 'urls':
                        $temp['replacement'] = sprintf('<a href="%s" target="_blank">%s</a>', $entity->url, $entity->display_url);

                        if($type === 'media') {
                            $this->media[] = $entity;
                        }
                        break;

                    case 'hashtags':
                        $temp['replacement'] = sprintf('<a href="https://twitter.com/hashtag/%s" target="_blank">#%s</a>', $entity->text, $entity->text);
                        break;

                    case 'user_mentions':
                        $temp['replacement'] = sprintf('<a href="https://twitter.com/intent/user/?user_id=%s" target="_blank">@%s</a>', $entity->id, $entity->screen_name);
                        break;

                    case 'symbols':
                        $temp['replacement'] = sprintf('<a href="https://twitter.com/search?q=\%24%s" target="_blank">$%s</a>', $entity->text, $entity->text);
                        break;

                    default:
                        $temp['replacement'] = sprintf('<a href="https://twitter.com/search?q=%s" target="_blank">%s</a>', $entity->text, $entity->text);
                        break;

                }

                $replacements[] = $temp;

            }

        }

        usort($replacements, function($a, $b) {
            $a = $a['start'];
            $b = $b['start'];

            return $b < $a ? -1 : ($b === $a ? 0 : 1);
        });

        $text = isset($tweet->full_text) ? $tweet->full_text : $tweet->text;

        foreach($replacements as $item) {
            $text = $this->mb_substr_replace($text, $item['replacement'], $item['start'], $item['end'] - $item['start']);
        }

        $this->tweet = $text;

        if(isset($tweet->retweeted_status) && $tweet->retweeted_status) {
            $this->retweet = new self($tweet->retweeted_status);
        }

        if(isset($tweet->quoted_status) && $tweet->quoted_status) {
            $this->quote = new self($tweet->quoted_status);
        }

        $this->user = $tweet->user;
    }

    public function getMedia(string $type = null) : array {

        if($type) {
            return array_filter($this->media, function($media) use($type) {
                return $media->type === $type;
            });
        }

        return $this->media;

    }

    public function hasMedia(string $type = null) : bool {

        return !empty($this->getMedia($type));

    }

    public function isRetweet() : bool {
        return !is_null($this->retweet);
    }

    public function isQuote(bool $useRetweet = true) : bool {


        if($this->isRetweet() && $useRetweet) {
            return $this->retweet->isQuote($useRetweet);
        }

        return !is_null($this->quote);

    }

    public function getTweet(bool $useRetweet = true) : string {

        if($this->isRetweet() && $useRetweet) {
            return $this->retweet->getTweet($useRetweet);
        }

        return $this->tweet;

    }

    public function getRetweet() {

        return $this->isRetweet() ? $this->retweet : null;

    }

    public function getQuote(bool $useRetweet = true) {

        if($this->isRetweet() && $useRetweet) {
            return $this->retweet->getQuote($useRetweet);
        }

        return $this->isQuote() ? $this->quote : null;

    }

    public function getUser(string $field = 'screen_name', bool $useRetweet = true) {

        if($field === 'link' || $field === 'url') {
            return $this->getUserLink($useRetweet);
        }

        if($this->isRetweet() && $useRetweet) {
            return $this->retweet->getUser($field, $useRetweet);
        }


        switch($field) {
            case 'link':
            case 'url':
                return $this->getUserLink(false);
                break;

            case 'full':
            case 'object':
                return $this->user;

            default:
                return isset($this->user->{$field}) ? $this->user->{$field} : null;
        }

    }

    public function getTweetId(bool $useRetweet = true) {

        if($this->isRetweet() && $useRetweet) {
            return $this->retweet->getTweetId($useRetweet);
        }

        return $this->id;
    }

    public function getTweetLink(bool $useRetweet = true) : string {

        $username = $this->getUser('screen_name', $useRetweet);
        $tweet_id = $this->getTweetId($useRetweet);

        return sprintf('https://twitter.com/%s/status/%s/', $username, $tweet_id);

    }

    public function getTime() : \DateTime {
        return $this->time;
    }

    public function getUserLink(bool $useRetweet = true) {
        $id = $this->getUser('id', $useRetweet);
        return sprintf('https://twitter.com/intent/user/?user_id=%s', $id);
    }

    public function mb_substr_replace(string $string, string $replace, int $offset, int $length = null) : string {

        $start = mb_substr($string, 0, $offset, 'UTF-8');
        $end = mb_substr($string, $offset+$length, mb_strlen($string), 'UTF-8');

        return $start.$replace.$end;

    }
}