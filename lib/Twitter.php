<?php namespace Dorigo\SocialAPI;

use Dorigo\SocialAPI\AbstractAPI;
use Dorigo\SocialAPI\SocialPost\Tweet;

class Twitter extends AbstractAPI {
    private $defaultOptions = [
        'trim_user' => false,
        'include_rts' => true,
        'exclude_replies' => false,
        'count' => 10,
        'tweet_mode' => 'extended'
    ];

    protected $apiKey;
    protected $apiSecret;

    protected function __construct($cacheExpiry = 3600) {
        parent::__construct($cacheExpiry, 'twitter');
    }

    protected function setClient() : void {

        $this->client = new \Freebird\Services\freebird\Client();
        $this->client->init_bearer_token($this->apiKey, $this->apiSecret);
    }

    public function setAPIKeys() : void {

        $this->apiKey = $this->getOption('api_key');
        $this->apiSecret = $this->getOption('secret_key');

    }

    protected function checkAPIKeys() : bool {
        if(is_null($this->apiKey) || is_null($this->apiSecret)) {
            return false;
        }

        return true;
    }

    public function get(string $username, array $options = []) : array {
        $options = array_merge($this->defaultOptions, $options);
        $options['screen_name'] = $username;

        $timeline = $this->cachedRequest('statuses/user_timeline.json', $options);

        return array_map(function($tweet) {
            return new Tweet($tweet);
        }, $timeline);

        /*return array_map(function($status_object) {
            $status_object = self::parseTweet($status_object);

            return $status_object;
        },$timeline);*/
    }

    public function request(string $endpoint, array $options = []) : array {
        $return = $this->client->api_request($endpoint, $options);
        return json_decode($return);
    }

    public function parseTweet($status_object, $quote = false) {
        $tweet = isset($status_object['retweeted_status']) ? $status_object['retweeted_status'] : $status_object;

        $text = (isset($tweet['full_text'])?$tweet['full_text']:$tweet['text']);
        $time = strtotime($tweet['created_at']);

        $entity_list = array();
        $media = array();

        foreach($tweet['entities'] as $entity_type => $entities) {
            foreach($entities as $entity) {
                $temp["start"] = $entity['indices'][0];
                $temp["end"] = $entity['indices'][1];

                if(isset($replacements)) { unset($replacements); }

                switch($entity_type) {
                    case 'media':
                        $replacement_string = '<a href="%s" target="_blank">%s</a>';
                        $replacements = array(
                            $entity['url'],
                            $entity['display_url']
                        );

                        $media[] = $entity;

                        break;
                    case 'urls':
                        $replacement_string = '<a href="%s" target="_blank">%s</a>';
                        $replacements = array(
                            $entity['url'],
                            $entity['display_url']
                        );

                        break;
                    case 'hashtags':
                        $replacement_string = '<a href="https://twitter.com/hashtag/%s" target="_blank">#%s</a>';
                        $replacements = array(
                            $entity['text'],
                            $entity['text']
                        );

                        break;

                    case 'user_mentions':
                        $replacement_string = '<a href="https://twitter.com/intent/user/?user_id=%s" target="_blank" class="js-window" data-height="600">@%s</a>';
                        $replacements = array(
                            $entity['id'],
                            $entity['screen_name']
                        );

                        break;
                    case 'symbols':
                        $replacement_string = '<a href="https://twitter.com/search?q=\%24%s" target="_blank">$%s</a>';
                        $replacements = array(
                            $entity['text'],
                            $entity['text']
                        );

                        break;
                    default:
                        $replacement_string = '<a href="https://twitter.com/search?q=%s" target="_blank" onclick="%s">%s</a>';
                }

                $replacements = isset($replacements) ? $replacements : array(
                    $entity['text'], $entity['text'],
                );

                $temp["replacement"] = vsprintf($replacement_string, $replacements);

                $entity_list[] = $temp;
            }
        }

        usort($entity_list, function($a,$b) {
            return($b["start"] - $a["start"]);
        });

        foreach($entity_list as $item) {
            $text = self::utf8_substr_replace($text, $item["replacement"], $item["start"], $item["end"] - $item["start"]);
        }

        return [
            'id' => $status_object['id'],
            'text' => $text,
            'media' => $media,
            'time' => $time,
            'quote' => isset($tweet['quoted_status']) ? self::parseTweet($tweet['quoted_status']) : null,
            'user' => $status_object['user'],
            'retweet' => isset($status_object['retweeted_status']) ? $status_object['retweeted_status']['user'] : false,
            'link' => "https://twitter.com/{$status_object['user']['screen_name']}/status/{$status_object['id']}/"
        ];
    }

    private static function utf8_substr_replace($original, $replacement, $position, $length) {
        $startString = mb_substr($original, 0, $position, "UTF-8");
        $endString = mb_substr($original, $position + $length, mb_strlen($original), "UTF-8");

        $out = $startString . $replacement . $endString;

        return $out;
    }
}